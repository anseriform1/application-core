import 'dart:convert';
import 'dart:core';

import 'package:enterprise_core/src/models/user_processes.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:enterprise_core/src/repo/gqlQueries.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

final headers = {'Accept': 'application/json'};

final postHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

final encoder = JsonEncoder((item) {
  if (item is DateTime) {
    return item.toIso8601String().substring(0, 10);
  }
  return item.toJson();
});

extension NullableGet on Map {
  dynamic get(Object key) {
    return this.containsKey(key) ? this[key] : null;
  }
}

class ProcessRepo {
  final String allProcessQuery;
  final String processQuery;
  final String fields;

  final _client = new http.Client();
  GraphQLClient gql;

  ProcessRepo({
    @required String host,
    @required String containerId,
    @required String user,
    String socketUrl,
    @required String instanceDetailsFragment,
  })  : allProcessQuery = ALL_PROCESS_QUERY + instanceDetailsFragment,
        processQuery = PROCESS_QUERY + instanceDetailsFragment,
        fields = instanceDetailsFragment {
    final HttpLink httpLink = HttpLink(
      httpClient: _client,
      uri: host,
    );

    final WebSocketLink websocketLink = socketUrl == null
        ? null
        : WebSocketLink(
            url: socketUrl,
            config: SocketClientConfig(
              autoReconnect: true,
              inactivityTimeout: Duration(seconds: 30),
            ),
          );

    gql = GraphQLClient(
      link: websocketLink == null ? httpLink : httpLink.concat(websocketLink),
      cache: InMemoryCache(storagePrefix: ""),
    );

    gql.cache?.restore();
  }

  BehaviorSubject<List<PAMProcess>> processStream = BehaviorSubject.seeded([]);
  BehaviorSubject<PAMProcess> processInstanceStream = BehaviorSubject();

  final _cachePeriod = Duration(seconds: 5);
  DateTime _nextScheduled = DateTime.now().subtract(Duration(days: 1));

  _updateProcesses(bool force) {
    debugPrint("Update the process list, force: $force");
    if (!force && _nextScheduled.isAfter(DateTime.now())) {
      debugPrint(
          "Process List is within cache period so don't update until $_nextScheduled");
//      return;
    }
    _nextScheduled = DateTime.now().add(_cachePeriod);
    Stream.fromFuture(gql.query(
      QueryOptions(
        document: allProcessQuery,
        fetchPolicy: FetchPolicy.cacheAndNetwork,
      ),
    )).doOnError((err) {
      debugPrint("Error $err");
    }).map((resp) {
      gql.cache?.save();
      if (resp.data == null) {
        checkGraphQlError(resp);
        debugPrint("No Data Returned");
        return List<PAMProcess>();
        //TODO: handle no data better
      }
      var processes = resp.data['processInstances'] as List<dynamic>;
      if (processes == null || processes.length == 0) {
        debugPrint("No Processes found for user");
        return List<PAMProcess>();
      }

      return processes.map((proc) {
        var process = PAMProcess();
        try {
          process = proc == null
              ? PAMProcess()
              : PAMProcess.fromGql(proc as Map<String, dynamic>);
        } catch (e) {
          debugPrint("Unable to Parse the data $e");
        }

        return process;
      }).toList();
    }).listen(
      (e) {
        processStream.add(e);
      },
      cancelOnError: true,
      onError: (e) {
        processStream.addError(e);
      },
    );
  }

  BehaviorSubject<PAMProcess> getProcess({int processInstanceId}) {
    debugPrint("Getting process $processInstanceId");
    Stream.fromFuture(gql.query(
      QueryOptions(
          document: processQuery,
          fetchPolicy: FetchPolicy.cacheAndNetwork,
          variables: {"instanceId": processInstanceId}),
    )).doOnError((err) {
      debugPrint("Error $err");
    }).map((resp) {
      gql.cache?.save();
      if (resp.data == null) {
        checkGraphQlError(resp);
        debugPrint("No Data Returned");
        return PAMProcess();
      }

      return PAMProcess.fromGql(resp.data['processInstance']);
    }).listen((proc) {
      processInstanceStream.add(proc);
    });

    return processInstanceStream;
  }

  BehaviorSubject<List<PAMProcess>> getProcessesByGroup(String username,
      {bool force = false}) {
    debugPrint("Getting processes for $username");
    _updateProcesses(force);
    return processStream;
  }

  //new method for getting the user process probably needs cleaning up
  BehaviorSubject<List<PAMProcess>> getProcessesByUser(String username,
      {bool force = false}) {
    _updateProcesses(force);
    return processStream.map((event) {
      return event
          .where((element) => element.processTask.taskActualOwner == username)
          .toList();
    }).shareValue();
  }

  BehaviorSubject<List<PAMTask>> getTasksByUser(String username,
      {bool force = false}) {
    _updateProcesses(force);
    return getProcessesByUser(username).map(
      (event) {
        return event.map((e) => e.processTask).toList();
      },
    ).shareValue();
  }

  BehaviorSubject<List<PAMTask>> getTasksForGroup(String group,
      {bool force = false}) {
    _updateProcesses(force);
    return processStream
        .map(
          (event) => event.map((e) => e.processTask).toList(),
        )
        .shareValue();
  }

  Stream<String> createUserProcessInstance(String processId, String username,
      {Map<String, dynamic> input}) {
    debugPrint("Creating instance with \n${encoder.convert(input)}",
        wrapWidth: 1024);

    return Stream.fromFuture(gql.mutate(
      new MutationOptions(
        variables: {
          "data": input,
          "user": username ?? "",
        },
        document: NEW_TASKGQL,
      ),
    )).map((resp) {
      checkGraphQlError(resp);
      if (resp.hasException) {
        throw resp.exception;
      }

      return resp.data['createInstance'].toString();
    });
  }

  Stream<String> createGroupProcessInstance(
      String processId, String groupName) {
    return createUserProcessInstance(processId, groupName);
  }

  Stream<Map<String, dynamic>> getTaskDetail(int taskId, String direction) {
    var data = {
      "taskId": taskId,
      "direction": direction,
    };

    var options = QueryOptions(
      document: GET_TASK,
      variables: data,
    );

    return Stream.fromFuture(
      gql.query(options).catchError((error) {
        debugPrint("Error Retrieving task $direction $error");
      }),
    ).map((resp) {
      checkGraphQlError(resp);

      return ((resp.data as Map<String, dynamic>)?.containsKey("taskData") ??
              false)
          ? resp.data['taskData']
          : {};
    });
  }

  Stream<Map<String, dynamic>> getTaskInput(int taskId) =>
      getTaskDetail(taskId, "input");

  Stream<Map<String, dynamic>> getTaskOutput(int taskId) =>
      getTaskDetail(taskId, "output");

  Stream<void> saveTask(Map<String, dynamic> data) {
    throw new UnimplementedError(
        "Save Task not yet supported, use Complete Task");
//    return Observable.fromFuture(_client
//        .put('$_host$GET_CONTAINER/$_containerId$GET_TASK$_taskId$OUTPUT',
//            body: encoder.convert(data), headers: postHeaders)
//        .catchError((error) {
//      debugPrint(error);
//    })).doOnError((error) {
//      debugPrint("Error Saving Task $error");
//    }).map((resp) {
//      return resp.statusCode == 200;
//    });
  }

  Stream<PAMProcess> completeTask(
          {Map<String, dynamic> data,
          String username,
          String taskName,
          int taskId}) =>
      updateTask(
          action: "completed",
          taskId: taskId,
          username: username,
          data: data,
          taskName: taskName);

  Stream<PAMProcess> startTask(
          {String username, String taskName, int taskId}) =>
      updateTask(
          action: "started",
          taskId: taskId,
          username: username,
          taskName: taskName);

  Stream<PAMProcess> claimTask(
          {String username, String taskName, int taskId}) =>
      updateTask(
          action: "claimed",
          taskId: taskId,
          username: username,
          taskName: taskName);

  Stream<PAMProcess> releaseTask(
          {String username, String taskName, int taskId}) =>
      updateTask(
          action: "released",
          taskId: taskId,
          username: username,
          taskName: taskName);

  Stream<PAMProcess> updateTask(
      {String action,
      int taskId,
      String username,
      Map<String, dynamic> data,
      @required String taskName}) {
    Map<String, dynamic> variables = {
      "action": action,
      "taskId": taskId,
      "user": username,
    };

    if (data != null) {
      variables["data"] = data;
      variables["data"].remove('TaskName');
      variables["data"].remove('NodeName');
      variables["data"].remove('Skippable');
      variables["data"].remove('ActorId');
      variables["data"].remove('GroupId');
    } else {
      data = {};
      variables["data"] = data;
    }

    debugPrint("About to $action task: $taskId");
    debugPrint(data.toString());

    return Stream.fromFuture(
      gql.mutate(
        MutationOptions(
            document: UPDATE_TASK(simplify(taskName)) + fields,
            variables: variables),
      ),
    ).map((resp) {
      checkGraphQlError(resp);
      return PAMProcess.fromGql(resp.data['update' + simplify(taskName)]);
    });
  }

  Stream<bool> escalateTask({int taskId, Map<String, dynamic> data}) {
    Map<String, dynamic> variables = {
      "taskId": taskId,
    };

    if (data != null) {
      variables["data"] = data;
      variables["data"].remove('TaskName');
      variables["data"].remove('NodeName');
      variables["data"].remove('Skippable');
      variables["data"].remove('ActorId');
      variables["data"].remove('GroupId');
    } else {
      data = {};
      variables["data"] = data;
    }

    debugPrint("About to escalate task: $taskId");
    debugPrint(data.toString());

    return Stream.fromFuture(
      gql.mutate(
        MutationOptions(document: ESCALATE_TASK, variables: variables),
      ),
    ).map((resp) {
      checkGraphQlError(resp);
      int respCode = resp.data["escalateTask"];
      return respCode >= 200 && respCode < 300;
    });
  }

  void close() {
    _client.close();
    processStream.close();
  }

  String simplify(String taskName) {
    return taskName.replaceAll(' ', '');
  }
}

void checkGraphQlError(QueryResult resp) {
  if (resp.hasException) {
    debugPrint("Error ${resp.exception}");
//    throw Future.error(resp.exception);
  }
}
