const ALL_PROCESS_QUERY = r'''
query getProcessInstances {
  processInstances(limit: 10) {
    processId
    processName
    processInstanceID
    processInstanceState
    instanceDetails {
      ...instanceFields
    }
    activeUserTasks {
      taskDescription
      taskName
      taskId
      taskParentId
      taskStatus
      taskInputData {
        ...taskFields
      }
    }
  }
}
''';

const PROCESS_QUERY = r'''
query getProcessInstance($instanceId: Int!) {
  processInstance(instanceId:$instanceId) {
    processId
    processName
    processInstanceID
    processInstanceState
    instanceDetails {
      ...instanceFields
    }
    activeUserTasks {
      taskDescription
      taskName
      taskId
      taskParentId
      taskStatus
      taskInputData {
        ...taskFields
      }
    }
  }
}
''';

const NEW_TASKGQL = r'''
mutation  CreateInstance($data: CreateInvoiceInput, $user: String!){
  createInstance(data: $data, user: $user)
}
''';

UPDATE_TASK(taskType) => """
mutation  UpdateTask(\$action: TaskAction!, \$taskId: Int!, \$data: ${taskType}Input!, \$user: String!){
  update${taskType}(action:\$action, taskInstanceId: \$taskId, input: \$data, user: \$user) {
    processId
    processName
    processInstanceID
    processInstanceState
    instanceDetails {
      ...instanceFields
    }
    activeUserTasks {
      taskDescription
      taskName
      taskId
      taskParentId
      taskStatus
      taskInputData {
        ...taskFields
      }
    }
  }
}
""";

const ESCALATE_TASK = r'''
mutation  EscalteTask($taskId: Int!, $data: TaskInput){
  escalateTask(taskId: $taskId, data: $data)
}
''';

const GET_TASK = r'''
query GetTask($taskId: Int!, $direction: String){
  taskData(taskId: $taskId, direction: $direction) {    
  }
}
''';
