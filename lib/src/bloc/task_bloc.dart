import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:enterprise_core/src/repo/api.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class TaskBloc {
  final ProcessRepo repo;
  final int taskId;
  final PAMTask taskSummary;
  final bool autoUpdate;
  UpdateFunc _update;
  Map<String, dynamic> inputs;

  final Map<String, BlocProperty> properties = {};
  final _taskOutputController = BehaviorSubject<Map<String, dynamic>>();
  final isLoadingStream = BehaviorSubject<bool>();
  final currentTaskSummaryStream = BehaviorSubject<PAMTask>();

  TaskBloc({
    @required this.repo,
    this.taskId,
    UpdateFunc update,
    this.autoUpdate = true,
    this.taskSummary,
  }) : _update = update {
    isLoadingStream.sink.add(true);
    currentTaskSummaryStream.sink.add(taskSummary);
    if (taskSummary?.data != null) {
//      setValue(taskSummary.data);
    } else if (taskId != null) {
      repo.getTaskInput(taskId).listen((input) {
        setValue(input);
        isLoadingStream.sink.add(false);
      });
    }

    _taskOutputController.listen((update) {
      debugPrint(update.toString());
    });

    if (autoUpdate)
      _taskOutputController.listen((update) => repo.saveTask(update));
  }

  BlocProperty<T> register<T>(
      {@required String property,
      T seed,
      ValidateFunc<T> validator,
      List<String> preRequisites}) {
    //TODO this does not work correctly
    if (_taskOutputController.hasValue) {
      var currentValues = _collapse(_taskOutputController.value, "", null);
      if (currentValues.containsKey(property) ?? false) {
        seed = currentValues[property];
      }
    }

    if (!properties.containsKey(property)) {
      properties[property] = BlocProperty<T>(
          seed: seed, validator: validator, preRequisites: preRequisites);
    }
    return properties[property];
  }

  Map<String, dynamic> _collapse(Map<String, dynamic> input, String parentKey,
      Map<String, dynamic> output) {
    if (output == null) output = Map<String, dynamic>();

    input.forEach((k, v) {
      if (k.contains(".")) k = k.replaceAll(".", "_");
      if (v is Map<String, dynamic>) {
        _collapse(v, parentKey == "" ? k : "$parentKey.$k", output);
      } else {
        if (parentKey == "") {
          output[k] = v;
        } else {
          output["$parentKey.$k"] = v;
        }
      }
    });
    return output;
  }

  void setValue(Map<String, dynamic> dataMap) {
    debugPrint("Setting Values ${dataMap.toString()}");
    _taskOutputController.add(dataMap);

    //TODO: I think we want to set properties here even if not already loaded

    if (dataMap != null) {
      dataMap = _collapse(dataMap, "", null);
      dataMap.forEach((key, val) {
        if (properties.containsKey(key)) {
          debugPrint('value in properties');
          final oldVal = properties[key].currentValue;
          if (oldVal != val) {
            if (properties[key] is BlocProperty<DateTime>) {
              if (val != "") {
                var dt = DateTime.parse(val);
                properties[key].valueSink.add(dt);
              }
            } else {
              properties[key].valueSink.add(val);
            }
          }
        }
      });
    }
  }

  void setIndividualValue(String key, dynamic value) {
    Map<String, dynamic> oldValue = {};

    if (_taskOutputController.value != null) {
      if (_taskOutputController.value != null) {
        oldValue = _taskOutputController.value;
      }
    }

    final newValue = oldValue..addAll({key: value});
    setValue(newValue);
  }

  Stream<void> startTask(String username) {
    return repo
        .startTask(
            username: username, taskName: taskSummary.taskName, taskId: taskId)
        .doOnData((_) {
      _handleStartCompletion();
    });
  }

  Stream<void> claimTask(String username) {
    return repo
        .claimTask(
            username: username, taskName: taskSummary.taskName, taskId: taskId)
        .doOnData((_) {
      _handleClaimCompletion();
    });
  }

  Stream<void> releaseTask(String username) {
    return repo
        .releaseTask(
            username: username, taskName: taskSummary.taskName, taskId: taskId)
        .doOnData((_) {
      _handleReleaseCompletion();
    });
  }

  void _handleStartCompletion() {
    debugPrint("Finished Starting Task");
    taskSummary.taskStatus = "InProgress";
    currentTaskSummaryStream.sink.add(taskSummary);
  }

  void _handleClaimCompletion() {
    debugPrint("Finished claiming Task");
    taskSummary.taskStatus = "Reserved";
    currentTaskSummaryStream.sink.add(taskSummary);
  }

  void _handleReleaseCompletion() {
    debugPrint("Finished Releasing Task");
    taskSummary.taskStatus = "Ready";
    currentTaskSummaryStream.sink.add(taskSummary);
  }

  Map<String, dynamic> setNestedValueIntoMap(
      List<String> keys, dynamic value, Map<String, dynamic> existingMap) {
    if (keys.length == 1) {
      //This is the leaf just set it
      existingMap[keys[0]] = (value is DateTime)
          ? value.toIso8601String().substring(0, 10)
          : value;
    } else {
      existingMap[keys[0]] = setNestedValueIntoMap(
          keys.sublist(1),
          value,
          (existingMap?.containsKey(keys[0]) ?? false)
              ? existingMap[keys[0]] ?? {}
              : {});
    }
    return existingMap;
  }

  Stream<Map<String, dynamic>> get outputStream =>
      _taskOutputController.map((event) {
        return mapFromValue(event);
      });

  Map<String, dynamic> _generateMapForSaving() {
    var map = _taskOutputController.value ?? Map<String, dynamic>();
    return mapFromValue(map);
  }

  Map<String, dynamic> mapFromValue(Map<String, dynamic> map) {
    //maps bloc properties which are written in dot notation into a nested map

    properties.forEach((key, prop) {
      if (key.contains(".")) {
        var keyParts = key.split(".");
        map = setNestedValueIntoMap(keyParts, prop.currentValue, map);
      } else {
        debugPrint("Setting $key to ${prop.currentValue}");
        map[key] = prop.currentValue;
      }
    });

    return map;
  }

  Map<String, dynamic> _generateMapForSubmission() {
    var map = _generateMapForSaving();
    return map;
  }

  Map<String, dynamic> getCurrentOutputs() {
    return _generateMapForSaving();
  }

  void saveTask() {
    final map = _generateMapForSaving();
    repo.saveTask(map).listen((_) => debugPrint("Saving Task"));
  }

  Stream<Map<String, dynamic>> createTask() {
    return Stream.value(_generateMapForSubmission());
  }

  Stream<PAMProcess> completeTask(String username) {
    isLoadingStream.add(true);
    final map = _generateMapForSubmission();
    return repo
        .completeTask(
            data: map,
            username: username,
            taskName: taskSummary.taskName,
            taskId: taskId)
        .map(
      (element) {
        isLoadingStream.add(false);
        return element;
      },
    );
  }

  Stream<bool> escalateTask(String username) {
    final map = _generateMapForSubmission();
    return repo.escalateTask(data: map);
  }

  void update() => setValue(_update());

  void close() {
    properties.forEach((key, prop) => prop.close());
  }
}

class BlocProperty<T> {
  final BehaviorSubject<T> _propertyValueController;
  ValidateFunc<T> validator = (val) => val;
  List<String> preRequisites;

  BlocProperty({T seed, ValidateFunc<T> validator, this.preRequisites})
      : _propertyValueController =
            seed == null ? BehaviorSubject() : BehaviorSubject<T>.seeded(seed) {
    if (validator != null) {
      this.validator = validator;
    }
  }

  Stream<T> get valueStream => _propertyValueController.map(validator);
  T get currentValue => _propertyValueController.value;
  Sink<T> get valueSink => _propertyValueController.sink;

  //TODO: Move this up to the Bloc = and then either use preRequisites or a custom method
  Stream<FormEntryVisibility> visibilityStream =
      Stream.value(FormEntryVisibility.ENABLED);

  void close() {
    _propertyValueController?.close();
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BlocProperty &&
          runtimeType == other.runtimeType &&
          _propertyValueController == other._propertyValueController &&
          validator == other.validator &&
          preRequisites == other.preRequisites &&
          visibilityStream == other.visibilityStream;

  @override
  int get hashCode =>
      _propertyValueController.hashCode ^
      validator.hashCode ^
      preRequisites.hashCode ^
      visibilityStream.hashCode;
}

typedef T ValidateFunc<T>(T input);
typedef Map<String, dynamic> UpdateFunc();

enum FormEntryVisibility {
  ENABLED,
  DISABLED,
  HIDDEN,
}
