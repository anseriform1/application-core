import 'dart:async';

import 'package:enterprise_core/src/models/user_processes.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:enterprise_core/src/repo/api.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ProcessBloc {
  final ProcessRepo repo;
  final String processId;
  final BehaviorSubject<List<PAMTask>> closedTasks =
      BehaviorSubject<List<PAMTask>>();
  final BehaviorSubject<List<PAMTask>> combinedTasks =
      BehaviorSubject<List<PAMTask>>();
  final BehaviorSubject<PAMTask> currentTask = BehaviorSubject<PAMTask>();

  final BehaviorSubject<List<PAMProcess>> combinedProcesses =
      BehaviorSubject<List<PAMProcess>>();
  final BehaviorSubject<PAMProcess> currentProcess =
      BehaviorSubject<PAMProcess>();
  StreamSubscription<List<PAMProcess>> _currentProcessSubscription;

  ProcessBloc({@required this.repo, @required this.processId});

  BehaviorSubject<List<PAMProcess>> getProcessesByUser(String username) {
    return repo.getProcessesByUser(username);
  }

  BehaviorSubject<PAMProcess> getProcessInstance(int processInstanceId) {
    return repo.getProcess(processInstanceId: processInstanceId);
  }

  BehaviorSubject<List<PAMProcess>> getProcessesByGroup(String group,
      {bool force = false}) {
    return repo.getProcessesByGroup(group, force: force);
  }

  BehaviorSubject<List<PAMTask>> getTasksByUser(String username) {
    return repo.getTasksByUser(username);
  }

  BehaviorSubject<List<PAMTask>> getTasksForGroup(String group) {
    return repo.getTasksForGroup(group);
  }

  StreamSubscription<List<PAMTask>> subscription;
  StreamSubscription<List<PAMProcess>> subscriptionProc;
  String prevUser;

  void getCombinedProcesses(String username, String group) {
    if (username != null && username != prevUser) {
      subscriptionProc?.cancel();
      prevUser = username;
      subscriptionProc = Rx.combineLatest2(
          repo.getProcessesByUser(username), repo.getProcessesByGroup(group),
          (List<PAMProcess> userData, List<PAMProcess> groupData) {
        debugPrint('Getting combinedProcesses ....................');
        final blankTask = PAMProcess.fromGql({});

        List<PAMProcess> combinedList = [
          blankTask,
          ...userData,
          blankTask,
          ...groupData
        ];
        return combinedList;
      }).listen(
        (data) {
          combinedProcesses.add(data);
          debugPrint("DEBUG combining returned");
        },
        onError: combinedTasks.addError,
      );
    }
  }

  void getCombinedTasks(String username, String group) {
    if (username != null && username != prevUser) {
      subscription?.cancel();
      prevUser = username;
      subscription = Rx.combineLatest2(
          repo.getTasksByUser(username), repo.getTasksForGroup(group),
          (List<PAMTask> userData, List<PAMTask> groupData) {
        final blankTask = PAMTask.fromJson({});

        List<PAMTask> combinedList = [
          blankTask,
          ...userData,
          blankTask,
          ...groupData
        ];
        return combinedList;
      }).listen(
        (data) {
          combinedTasks.add(data);
          debugPrint("DEBUG combining returned");
        },
        onError: (error) {
          debugPrint("Error getting tasks $error");
          combinedTasks.addError(error);
        },
      );
    }
  }

  Stream<String> createUserProcessInstance(String username, String group,
      {Map<String, dynamic> data}) {
    debugPrint(
        "Creating new process instance of $processId with ${data} for $username");
    return repo
        .createUserProcessInstance(processId, username, input: data)
        .doOnData((data) {
      debugPrint("Successfully created new $processId ID: $data");
    }).doOnError((error) {
      debugPrint("Error creating process instance $error");
    });
  }

  void createGroupProcessInstance(String groupName) {
    debugPrint("Creating new group process instance of $processId");
    repo.createGroupProcessInstance(processId, groupName).listen((data) {
      debugPrint("Successfully created new $processId - result was $data");
//      getTasksForGroup(groupName);
    });
  }

  void selectTask(PAMTask task) {
    currentTask.add(task);
  }

  void selectProcess(PAMProcess process) {
    _currentProcessSubscription?.cancel();
    currentProcess.add(process);
    if (process == null) {
      return;
    }
    _currentProcessSubscription = getProcessesByGroup("").listen((event) {
      debugPrint("Processes updated: ${event.length}");
      var updatedProcess = event?.firstWhere(
        (element) =>
            element != null && element?.instanceId == process?.instanceId,
        orElse: () => null,
      );

      if (updatedProcess?.processTask?.taskName !=
          currentProcess.value?.processTask?.taskName) {
        debugPrint(
            "Current Process updated: ${updatedProcess?.processTask?.taskName}");
        currentProcess.add(updatedProcess);
      }
    });
  }

  void markTaskCompleted(username) {
    if (currentTask.value == null) {
      currentTask.add(getTasksForGroup("").value[0]);
    } else {
      currentTask.add(null);
    }
//    getTasksByUser(username);
  }

  void close() {
    currentProcess.close();
  }
}
