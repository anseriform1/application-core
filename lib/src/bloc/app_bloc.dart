import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AppBloc {
  String username;
  String group;
  String company;

  AppBloc({this.username, this.group});

  BehaviorSubject<bool> isLoggedInStream = BehaviorSubject<bool>.seeded(false);

  void logout() {
    username = null;
    group = null;
    company = null;
    isLoggedInStream.sink.add(false);
  }

  void setUsername(String username) {
    this.username = username;
    isLoggedInStream.sink.add(true);
  }

  void setGroup(String group) {
    this.group = group;
    isLoggedInStream.sink.add(true);
  }

  void setData(String username, String group, String company) {
    debugPrint("Setting username: $username and group $group at $company");
    this.username = username;
    this.group = group;
    this.company = company;
    isLoggedInStream.sink.add(true);
  }

  void close() {}
}
