import 'package:enterprise_core/src/components/basic_button.dart';
import 'package:enterprise_core/src/components/enterprise_app.dart';
import 'package:enterprise_core/src/components/enterprise_process.dart';
import 'package:enterprise_core/src/components/enterprise_task.dart';
import 'package:flutter/material.dart';

enum TaskFlow { STANDARD, SIMPLIFIED }

class ActionButtons extends StatelessWidget {
  final TaskFlow flow;

  const ActionButtons({
    Key key,
    this.flow = TaskFlow.STANDARD,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var taskBloc = EnterpriseTask.bloc(context);
    var processBloc = EnterpriseProcess.bloc(context);
    var appBloc = EnterpriseApp.bloc(context);
    debugPrint("Action Button Status ${taskBloc.taskSummary?.taskStatus}");
    debugPrint("Action Button AppUsername ${appBloc.username}");

    var status = taskBloc.taskSummary?.taskStatus ?? "New";

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        if (status == "New")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                processBloc
                    .createUserProcessInstance(
                  appBloc.username,
                  appBloc.group,
                  data: taskBloc.getCurrentOutputs(),
                )
                    .listen((event) {
                  Scaffold.of(context).showSnackBar(new SnackBar(
                    duration: Duration(milliseconds: 250),
                    content: new Text(
                        "Task Created ${taskBloc.taskSummary?.taskName ?? "New Task"}"),
                  ));
                }).onError((error) {
                  Scaffold.of(context).showSnackBar(new SnackBar(
                    duration: Duration(milliseconds: 250),
                    content: new Text(
                        "Error Creating Task ${taskBloc.taskSummary?.taskName ?? "New Task"} => $error}"),
                  ));
                });
              },
              child: Text("Create"),
            ),
          ),
        if (status == "Reserved")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                taskBloc.startTask(appBloc.username).listen((_) {
//                  processBloc.refreshAllTasks(appBloc.username, appBloc.company);
                });
              },
              child: Text("Start"),
            ),
          ),
        if (status == "Reserved")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                taskBloc.releaseTask(appBloc.username).listen((_) {
//                  processBloc.refreshAllTasks(appBloc.company, appBloc.company);
                  Scaffold.of(context).showSnackBar(new SnackBar(
                    duration: Duration(milliseconds: 250),
                    content: new Text(
                        "Released Task ${taskBloc.taskSummary.taskName}"),
                  ));
                });
              },
              child: Text("Release"),
            ),
          ),
//        if (status == "InProgress")
//          Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: BasicButton(
//              width: 150,
//              onPressed: () {
//                taskBloc.saveTask();
//              },
//              child: Text("Save"),
//            ),
//          ),
        if (status == "InProgress")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                taskBloc.releaseTask(appBloc.username).listen((_) {
//                  processBloc.refreshAllTasks(appBloc.company, appBloc.company);
                  Scaffold.of(context).showSnackBar(new SnackBar(
                    duration: Duration(milliseconds: 250),
                    content: new Text(
                        "Released Task ${taskBloc.taskSummary.taskName}"),
                  ));
                });
              },
              child: Text("Release"),
            ),
          ),
        if (status == "InProgress")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                taskBloc.completeTask(appBloc.username).listen((success) {
                  processBloc.markTaskCompleted(appBloc.username);
                  Scaffold.of(context).showSnackBar(
                    new SnackBar(
                      duration: Duration(milliseconds: 250),
                      content: new Text(
                          "Completed Task ${taskBloc.taskSummary.taskName}"),
                    ),
                  );
//                  processBloc.refreshAllTasks(appBloc.company, appBloc.company);
                });
              },
              child: Text("Complete"),
            ),
          ),
        if (status == "Ready")
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: BasicButton(
              width: 150,
              onPressed: () {
                taskBloc.claimTask(appBloc.username).listen((_) {
//                  processBloc.refreshAllTasks(appBloc.company, appBloc.company);
                  Scaffold.of(context).showSnackBar(new SnackBar(
                    duration: Duration(milliseconds: 250),
                    content:
                        new Text("Claimed Task ${taskBloc.taskSummary.taskId}"),
                  ));
                });
              },
              child: Text(
                "Claim",
              ),
            ),
          ),
      ],
    );
  }
}
