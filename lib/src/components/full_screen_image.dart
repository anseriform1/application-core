import 'package:flutter/material.dart';

class FullScreenImage extends StatelessWidget {
  final String imageSource;
  final Color primaryColor;

  FullScreenImage(this.imageSource, this.primaryColor);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 0.0,
      backgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          leading: FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.close),
          ),
          title: Text("View Image"),
          centerTitle: true,
        ),
        body: Center(
          child: Image.network(
            imageSource,
            fit: BoxFit.contain,
            alignment: Alignment.center,
            width: double.infinity,
            height: double.infinity,
          ),
        ),
      ),
    );
  }
}
