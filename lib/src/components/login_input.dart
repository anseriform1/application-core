import 'package:enterprise_core/src/components/enterprise_app.dart';
import 'package:enterprise_core/src/components/screen_size.dart';
import 'package:flutter/material.dart';

class LoginInputField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;

  LoginInputField({
    Key key,
    @required this.controller,
    @required this.labelText,
  }) : super(key: key);

  final screen = ScreenSize();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: screen.useMobileLayout(context) == false
          ? const EdgeInsets.only(left: 108, right: 108, top: 20, bottom: 15)
          : const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 10),
      child: TextField(
        style: TextStyle(
            fontSize: 14,
            color:
                EnterpriseApp.of(context).enterpriseTheme.primaryButtonColour),
        keyboardType: TextInputType.text,
        controller: controller,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: 20, bottom: 5),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black.withOpacity(0.25))),
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black.withOpacity(0.07))),
          labelStyle: TextStyle(
            color: Color(0xFF1d1d26).withOpacity(0.4),
            fontSize: 10,
          ),
          labelText: labelText,
//                      errorText: snapshot.error?.toString(),
        ),
      ),
    );
  }
}
