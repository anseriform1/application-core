import 'package:flutter/material.dart';

class ScreenSize {
  bool useMobileLayout(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return shortestSide < 600;
  }
}
