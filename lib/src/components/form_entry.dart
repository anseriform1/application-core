import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/bloc/task_bloc.dart';
import 'package:enterprise_core/src/components/enterprise_task.dart';
import 'package:enterprise_core/src/components/error_label.dart';
import 'package:flutter/material.dart';

class FormEntry<U> extends StatelessWidget {
  final String property;
  final U seed;
  final ValidateFunc<U> validator;
  final List<String> preRequisites = [];
  final FormEntryBuilder<U> builder;

  static FormEntryBuilder hiddenInputBuilder() =>
      (context, prop, {controller}) {
        return Container();
      };

  static FormEntryBuilder multiLineTextInputBuilder(fieldLabel, lines) =>
      (context, prop, {controller}) {
        return StreamBuilder<String>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  style:
                      EnterpriseApp.of(context).enterpriseTheme.inputTextStyle,
                  controller: controller,
                  onChanged: prop.valueSink.add,
                  keyboardType: TextInputType.multiline,
                  maxLines: lines,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.black.withOpacity(0.15))),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.black.withOpacity(0.07))),
                      labelStyle: EnterpriseApp.of(context)
                          .enterpriseTheme
                          .inputTextLabelStyle,
                      labelText: fieldLabel.toString().toUpperCase(),
                      errorText: snapshot.error?.toString()),
                ),
              );
            });
      };

  static FormEntryBuilder multiLineOutlinedTextInputBuilder(fieldLabel, lines, colour) =>
          (context, prop, {controller}) {
        return StreamBuilder<String>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[ Padding(
              padding: const EdgeInsets.only(top:10.0, bottom: 15), child: Text("Comments:",
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(fontWeight: FontWeight.w500, color: colour))), Padding(
                padding: const EdgeInsets.only(right:1.0),
                child: TextField(
                  style:
                  EnterpriseApp.of(context).enterpriseTheme.inputTextStyle,
                  controller: controller,
                  onChanged: prop.valueSink.add,
                  keyboardType: TextInputType.multiline,
                  maxLines: lines,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 10, top: 15, bottom: 0, right: 10),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(6.0),
                      ),
                      borderSide: BorderSide(color: colour, width: 1.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(6.0),
                      ),
                      borderSide: BorderSide(color: colour, width: 1.0),
                    ),
                  ),
                ),
              )]);
            });
      };

  static FormEntryBuilder readOnlyBuilder(fieldLabel, {isError = false}) =>
      (context, prop, {controller}) {
        return StreamBuilder(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              var label = snapshot.data?.toString() ?? '';
              if (snapshot.data is DateTime) {
                label = snapshot.data.toIso8601String().substring(0, 10);
              }
              if (isError && !snapshot.hasData) return Container();
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: InputDecorator(
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        labelText: fieldLabel,
                        errorText: snapshot.error?.toString()),
                    child: Text(
                      label,
                      style: isError
                          ? Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(color: Colors.red)
                          : Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
              );
            });
      };

  static FormEntryBuilder calculatorBuilder(fieldLabel, calculator) =>
      (context, prop, {controller}) {
        final taskBloc = EnterpriseTask.bloc(context);

        var calculateVal = calculator(taskBloc.getCurrentOutputs());
        prop.valueSink.add(calculateVal);

        taskBloc.outputStream.listen((event) {
          var calculateVal = calculator(event);
          prop.valueSink.add(calculateVal);
        });

        return StreamBuilder<dynamic>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return ErrorLabel(error: snapshot.error);
              }
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Text(
                    snapshot.data?.toString() ?? 'empty',
                    textAlign: TextAlign.center,
                  ),
//                  InputDecorator(
//                    decoration: InputDecoration(
//                        border: InputBorder.none,
//                        labelText: fieldLabel,
//                        errorText: snapshot.error?.toString()),
//                    child: Text(value),
//                  ),
                ),
              );
            });
      };

  static FormEntryBuilder textInputBuilder(fieldLabel) =>
      (context, prop, {controller}) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            style: EnterpriseApp.of(context).enterpriseTheme.inputTextStyle,
            controller: controller,
            onChanged: prop.valueSink.add,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            onSubmitted: (term) {
              FocusScope.of(context).nextFocus();
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(top: 20, bottom: 15),
              focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.black.withOpacity(0.15))),
              enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.black.withOpacity(0.07))),
              labelStyle:
                  EnterpriseApp.of(context).enterpriseTheme.inputTextLabelStyle,
              labelText: fieldLabel.toString().toUpperCase(),
//              errorText: snapshot.error?.toString(),
            ),
          ),
        );
      };

  static FormEntryBuilder textInputInlineBuilder(fieldLabel) =>
      (context, prop, {controller}) {
        return StreamBuilder<String>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.only(
                    left: 8.0, bottom: 0.0, right: 8.0, top: 24),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 150,
                      child: Text(
                        fieldLabel.toString().toUpperCase(),
                        textAlign: TextAlign.left,
                        style: EnterpriseApp.of(context)
                            .enterpriseTheme
                            .sidebarHeadingSubtitleStyle,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 36,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: TextField(
                            style: EnterpriseApp.of(context)
                                .enterpriseTheme
                                .inputTextStyle,
                            controller: controller,
                            onChanged: prop.valueSink.add,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            onSubmitted: (term) {
                              FocusScope.of(context).nextFocus();
                            },
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.only(
                                    top: 20, bottom: 15, left: 15, right: 15),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.15))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.07))),
                                labelStyle: EnterpriseApp.of(context)
                                    .enterpriseTheme
                                    .inputTextLabelStyle,
                                errorText: snapshot.error?.toString()),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            });
      };

  static FormEntryBuilder numberInputBuilder(fieldLabel) =>
      (context, prop, {controller}) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: controller,
            onChanged: (value) {
              var number = num.parse(value);
              if (number != prop.currentValue) {
                debugPrint(
                    "Update Value from ${prop.currentValue} to ${number}");
                prop.valueSink.add(number);
              }
            },
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            onSubmitted: (term) {
              FocusScope.of(context).nextFocus();
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(top: 20, bottom: 15),
              focusedBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.black.withOpacity(0.15))),
              enabledBorder: UnderlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.black.withOpacity(0.07))),
              labelStyle:
                  EnterpriseApp.of(context).enterpriseTheme.inputTextLabelStyle,
              labelText: fieldLabel.toString().toUpperCase(),
//                errorText: snapshot.error?.toString(),
            ),
          ),
        );
      };

  static FormEntryBuilder numberInputInlineBuilder(fieldLabel) =>
      (context, prop, {controller}) {
        return StreamBuilder<num>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.only(
                    left: 8.0, bottom: 0.0, right: 8.0, top: 24),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 150,
                      child: Text(
                        fieldLabel.toString().toUpperCase(),
                        textAlign: TextAlign.left,
                        style: EnterpriseApp.of(context)
                            .enterpriseTheme
                            .sidebarHeadingSubtitleStyle,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 36,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: TextField(
                            style: EnterpriseApp.of(context)
                                .enterpriseTheme
                                .inputTextStyle,
                            controller: controller,
                            onChanged: (value) {
                              var number = num.parse(value);
                              if (number != prop.currentValue) {
                                prop.valueSink.add(number);
                              }
                            },
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                contentPadding: EdgeInsets.only(
                                    top: 20, bottom: 15, left: 15, right: 15),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.15))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black.withOpacity(0.07))),
                                labelStyle: EnterpriseApp.of(context)
                                    .enterpriseTheme
                                    .inputTextLabelStyle,
                                errorText: snapshot.error?.toString()),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            });
      };

  static FormEntryBuilder datePickerBuilder(fieldLabel) =>
      (context, prop, {controller}) {
        return StreamBuilder<DateTime>(
            stream: prop.valueStream,
            builder: (context, snapshot) {
              return Theme(
                data: Theme.of(context).copyWith(
                  primaryTextTheme: Typography.material2018().white,
                  primaryColor: EnterpriseApp.of(context)
                      .enterpriseTheme
                      .datePickerBkgdColour, //color of the main banner
                  accentColor: EnterpriseApp.of(context)
                      .enterpriseTheme
                      .datePickerBkgdColour, //color of circle indicating the selected date
                  buttonTheme: ButtonThemeData(
                      textTheme: ButtonTextTheme
                          .accent //color of the text in the button "OK/CANCEL"
                      ),
                ),
                child: Builder(builder: (context) {
                  return GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () async {
                      DateTime date = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime.utc(1900, 1, 1),
                          lastDate: DateTime.now());
                      prop.valueSink.add(date);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, bottom: 0.0, right: 8.0, top: 24),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: 80,
                            child: Text(
                              fieldLabel.toString().toUpperCase(),
                              textAlign: TextAlign.left,
                              style: EnterpriseApp.of(context)
                                  .enterpriseTheme
                                  .sidebarHeadingSubtitleStyle,
                            ),
                          ),
                          Expanded(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth: 150,
                                minWidth: 50,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 20.0),
                                child: InputDecorator(
                                  decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      contentPadding: const EdgeInsets.only(
                                          left: 15.0,
                                          right: 15.0,
                                          top: 15.0,
                                          bottom: 10.0),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black
                                                  .withOpacity(0.15))),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black
                                                  .withOpacity(0.07))),
                                      labelStyle: EnterpriseApp.of(context)
                                          .enterpriseTheme
                                          .inputTextLabelStyle,
                                      errorText: snapshot.error?.toString()),
                                  child: Text(
                                    (snapshot.hasData)
                                        ? snapshot.data
                                            .toIso8601String()
                                            .substring(0, 10) // Just show date
                                        : '',
                                    style: EnterpriseApp.of(context)
                                        .enterpriseTheme
                                        .inputTextStyle,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
              );
            });
      };

  static FormEntryBuilder dropdownBuilder(
          String fieldLabel, List<String> dropdownOptions) =>
      (context, prop, {controller}) {
        return Padding(
          padding: const EdgeInsets.only(left: 9.0, right: 9.0),
          child: StreamBuilder<String>(
              stream: prop.valueStream,
              builder: (context, snapshot) {
                return InputDecorator(
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.black.withOpacity(0.07))),
                    labelStyle: EnterpriseApp.of(context)
                        .enterpriseTheme
                        .dropDownLabelStyle,
                    labelText: fieldLabel.toUpperCase(),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: Theme(
                      data:
                          Theme.of(context).copyWith(canvasColor: Colors.white),
                      child: DropdownButton<String>(
                        elevation: 3,
                        iconSize: 0,
                        hint: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                  getValueForDropDown(
                                      prop.currentValue.toString()),
                                  style: EnterpriseApp.of(context)
                                      .enterpriseTheme
                                      .dropDownTextStyle),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.arrow_drop_down,
                                color: EnterpriseApp.of(context)
                                    .enterpriseTheme
                                    .primaryButtonColour,
                              ),
                            )
                          ],
                        ),
                        isExpanded: true,
                        isDense: true,
                        items: dropdownOptions.map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value,
                                style: EnterpriseApp.of(context)
                                    .enterpriseTheme
                                    .dropDownTextStyle),
                          );
                        }).toList(),
                        onChanged: prop.valueSink.add,
                      ),
                    ),
                  ),
                );
              }),
        );
      };

  static FormEntryBuilder dropdownInlineBuilder(
          String fieldLabel, List<String> dropdownOptions) =>
      (context, prop, {controller}) {
        return Padding(
          padding: const EdgeInsets.only(left: 9.0, right: 9.0),
          child: StreamBuilder<String>(
              stream: prop.valueStream,
              builder: (context, snapshot) {
                return Row(
                  children: <Widget>[
                    Container(
                      width: 150,
                      child: Text(
                        fieldLabel.toString().toUpperCase(),
                        textAlign: TextAlign.left,
                        style: EnterpriseApp.of(context)
                            .enterpriseTheme
                            .sidebarHeadingSubtitleStyle,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 55,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: InputDecorator(
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black.withOpacity(0.07))),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: Theme(
                                data: Theme.of(context)
                                    .copyWith(canvasColor: Colors.white),
                                child: DropdownButton<String>(
                                  elevation: 3,
                                  iconSize: 0,
                                  hint: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                            getValueForDropDown(
                                                prop.currentValue.toString()),
                                            style: EnterpriseApp.of(context)
                                                .enterpriseTheme
                                                .dropDownTextStyle),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Image.network(
                                          "assets/images/angle-down.png",
                                        ),
                                      )
                                    ],
                                  ),
                                  isExpanded: true,
                                  isDense: true,
                                  items: dropdownOptions.map((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value,
                                          style: EnterpriseApp.of(context)
                                              .enterpriseTheme
                                              .dropDownTextStyle),
                                    );
                                  }).toList(),
                                  onChanged: prop.valueSink.add,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }),
        );
      };

  static FormEntryBuilder radioBuilder(
          String fieldLabel, List<String> options) =>
      (context, prop, {controller}) {
        return Padding(
          padding: const EdgeInsets.only(
              left: 10.0, top: 8.0, right: 8.0, bottom: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  fieldLabel.toUpperCase(),
                  style: EnterpriseApp.of(context)
                      .enterpriseTheme
                      .inputTextLabelStyle,
                ),
              ),
              StreamBuilder<String>(
                  stream: prop.valueStream,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: options.map((option) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Radio(
                              onChanged: prop.valueSink.add,
                              value: option,
                              groupValue: snapshot.data,
                              activeColor: EnterpriseApp.of(context)
                                  .enterpriseTheme
                                  .radioButtonColor,
                            ),
                            Text(
                              option,
                              style: EnterpriseApp.of(context)
                                  .enterpriseTheme
                                  .inputTextStyle,
                            )
                          ],
                        );
                      }).toList(),
                    );
                  }),
            ],
          ),
        );
      };

  FormEntry(
      {@required this.property,
      this.seed,
      this.validator,
      @required this.builder});

  factory FormEntry.ReadOnly(
      {@required property,
      @required fieldLabel,
      seed,
      validator,
      isError = false}) {
    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: readOnlyBuilder(fieldLabel, isError: isError),
    );
  }

  factory FormEntry.Hidden({
    @required property,
    seed,
    validator,
  }) {
    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: hiddenInputBuilder(),
    );
  }

  factory FormEntry.Calculated(
      {@required property,
      @required fieldLabel,
      @required bool write,
      @required FieldCalculator calculator}) {
//    if (U != String) {
//      throw Exception("Type on Calculated must be of type String");
//    }

    return FormEntry(
      property: property,
      builder: calculatorBuilder(fieldLabel, calculator),
    );
  }

  factory FormEntry.TextInput(
      {@required property, @required fieldLabel, seed, validator}) {
    if (U != String) {
      throw Exception("Type on TextInput must be of type String");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: textInputBuilder(fieldLabel),
    );
  }

  factory FormEntry.InlineTextInput(
      {@required property, @required fieldLabel, seed, validator}) {
    if (U != String) {
      throw Exception("Type on TextInput must be of type String");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: textInputInlineBuilder(fieldLabel),
    );
  }

  factory FormEntry.MultiLineTextInput(
      {@required property,
      @required fieldLabel,
      seed,
      validator,
      @required linesInInput}) {
    if (U != String) {
      throw Exception("Type on TextInput must be of type String");
    }

    return FormEntry(
        property: property,
        seed: seed,
        validator: validator,
        builder: multiLineTextInputBuilder(fieldLabel, linesInInput));
  }

  factory FormEntry.MultiLineOutlinedTextInput(
      {@required property,
        @required fieldLabel,
        seed,
        validator,
        @required linesInInput,
      @required colour}) {
    if (U != String) {
      throw Exception("Type on TextInput must be of type String");
    }

    return FormEntry(
        property: property,
        seed: seed,
        validator: validator,
        builder: multiLineOutlinedTextInputBuilder(fieldLabel, linesInInput, colour));
  }

  factory FormEntry.NumberInput(
      {@required property, @required fieldLabel, seed, validator}) {
    if (U != num) {
      throw Exception("Type on NumberInput must be of type num");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: numberInputBuilder(fieldLabel),
    );
  }

  factory FormEntry.InlineNumberInput(
      {@required property, @required fieldLabel, seed, validator}) {
    if (U != num) {
      throw Exception("Type on NumberInput must be of type num");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: numberInputInlineBuilder(fieldLabel),
    );
  }

  factory FormEntry.DatePicker(
      {@required property, @required fieldLabel, seed, validator}) {
    if (U != DateTime) {
      throw Exception("Type on DatePicker must be of type DateTime");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: datePickerBuilder(fieldLabel),
    );
  }

  factory FormEntry.Dropdown(
      {@required property,
      @required fieldLabel,
      @required List<String> dropdownOptions,
      seed,
      validator}) {
    if (U != String) {
      throw Exception("Type on Dropdown must be of type String");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: dropdownBuilder(fieldLabel, dropdownOptions),
    );
  }

  factory FormEntry.InlineDropdown(
      {@required property,
      @required fieldLabel,
      @required List<String> dropdownOptions,
      seed,
      validator}) {
    if (U != String) {
      throw Exception("Type on Dropdown must be of type String");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: dropdownInlineBuilder(fieldLabel, dropdownOptions),
    );
  }

  factory FormEntry.RadioButton(
      {@required property,
      @required fieldLabel,
      @required options,
      seed,
      validator}) {
    if (U != String) {
      throw Exception("Type on RadioButton must be of type String");
    }

    return FormEntry(
      property: property,
      seed: seed,
      validator: validator,
      builder: radioBuilder(fieldLabel, options),
    );
  }

  @override
  Widget build(BuildContext context) {
    var bloc = EnterpriseTask.bloc(context);
    var blocProperty = bloc.register<U>(
        property: property,
        seed: seed,
        validator: validator,
        preRequisites: preRequisites);

    return StatefullFormField(
      prop: blocProperty,
      builder: builder,
    );
  }

  static String getValueForDropDown(String value) {
    String displayValue;

    if (value == "null") {
      displayValue = "";
    } else {
      displayValue = value;
    }

    return displayValue;
  }
}

class StatefullFormField<T> extends StatefulWidget {
  final BlocProperty<T> prop;
  final FormEntryBuilder builder;

  const StatefullFormField({Key key, this.prop, this.builder})
      : super(key: key);

  @override
  _StatefullFormFieldState createState() => _StatefullFormFieldState();
}

class _StatefullFormFieldState extends State<StatefullFormField> {
  TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return widget.builder(context, widget.prop, controller: controller);
  }

  @override
  void initState() {
    super.initState();
    widget.prop.valueStream.listen((next) {
      var txt = next.toString();
      if (txt != controller?.text) {
        controller?.text = next.toString();
      }
    });

    if(widget.prop.currentValue.runtimeType == String) {
      controller = TextEditingController()..text = widget.prop.currentValue;
    }
  }
}

typedef Widget FormEntryBuilder<T>(BuildContext context, BlocProperty<T> prop,
    {TextEditingController controller});
typedef Z FieldCalculator<Z>(Map<String, dynamic> map);
enum FormEntryType { text, date, dropdown }
