import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/components/action_buttons.dart';
import 'package:enterprise_core/src/components/enterprise_task.dart';
import 'package:enterprise_core/src/components/screen_size.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:flutter/material.dart';

class FormCard extends StatelessWidget {
  final List<Widget> children;
  final Widget child;
  String title;
  final bool showActionButtons;

  FormCard({
    this.children,
    this.child,
    this.title = "",
    this.showActionButtons = true,
  });

  @override
  Widget build(BuildContext context) {
    final screen = ScreenSize();
    final bloc = EnterpriseTask.bloc(context);

    var group = EnterpriseApp.bloc(context).group;
    var isAdmin = {"Admin", "Retailer"}.contains(group);
    double sideBarWidth = 300;
    return Padding(
      padding: const EdgeInsets.only(top: 25.0, left: 25, right: 25),
      child: StreamBuilder<PAMTask>(
          stream: bloc.currentTaskSummaryStream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }
            final task = snapshot.data;
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          (title.isEmpty == true)
                              ? task?.taskName ?? "New"
                              : this.title,
                          style: EnterpriseApp.of(context)
                              .enterpriseTheme
                              .titleTextStyle
                              .copyWith(fontSize: 16.0),
                        ),
                        Text(
                          " - Task # ${task?.taskId?.toString() ?? "New"}",
                          style: EnterpriseApp.of(context)
                              .enterpriseTheme
                              .subtitleTextStyle
                              .copyWith(fontSize: 13.0),
                        ),
                        if (!isAdmin)
                          Expanded(
                            child: Container(
                              alignment: Alignment.centerRight,
                              child: FlatButton(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                        task != null ? "Create New" : "Cancel"),
                                    Icon(
                                        task != null ? Icons.add : Icons.close),
                                  ],
                                ),
                                onPressed: () => EnterpriseProcess.bloc(context)
                                    .markTaskCompleted(""),
                              ),
                            ),
                          )
                      ],
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.loose,
                    child: child == null
                        ? Container(
                            width: screen.useMobileLayout(context) == true
                                ? MediaQuery.of(context).size.width
                                : 700,
                            child: ListView(
                              children: [
                                ...children,
                              ],
                            ),
                          )
                        : SingleChildScrollView(
                            child: child,
                          ),
                  ),
                  if (showActionButtons) ActionButtons(),
                ],
              ),
            );
          }),
    );
  }
}

class FormRow extends StatelessWidget {
  final String title;
  final List<Widget> children;

  const FormRow({Key key, this.children, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var isMobile = ScreenSize().useMobileLayout(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        if (title != null)
          Text(
            title,
            style: EnterpriseApp.of(context).enterpriseTheme.titleTextStyle,
          ),
        if (isMobile) ...children,
        if (!isMobile)
          Row(
            children: <Widget>[...children.map((e) => Expanded(child: e))],
          ),
      ],
    );
  }
}
