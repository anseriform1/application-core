import 'package:flutter/material.dart';

class ErrorLabel extends StatelessWidget {
  final Object error;

  const ErrorLabel({Key key, this.error}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      "Error: $error",
      style: Theme.of(context).textTheme.headline4.copyWith(color: Colors.red),
    );
  }
}
