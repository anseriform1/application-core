import 'package:enterprise_core/src/bloc/app_bloc.dart';
import 'package:enterprise_core/src/models/enterprise_theme.dart';
import 'package:flutter/material.dart';

//TODO: If we don't need to do anything on dispose of the widget, we can just make this an inherited widget.

class EnterpriseApp extends StatefulWidget {
  EnterpriseApp({
    Key key,
    @required this.child,
    @required this.config,
  }) : super(key: key);

  final Widget child;
  final AppConfig config;

  @override
  _EnterpriseAppState createState() => _EnterpriseAppState();

  static AppConfig of(BuildContext context) {
    _EnterpriseAppInherited provider = context
        .ancestorInheritedElementForWidgetOfExactType(_EnterpriseAppInherited)
        ?.widget;
    return provider?.config;
  }

  static AppBloc bloc(BuildContext context) {
    _EnterpriseAppInherited provider = context
        .ancestorInheritedElementForWidgetOfExactType(_EnterpriseAppInherited)
        ?.widget;

    if (provider == null) {
      throw ProviderMissingException;
    }

    return provider?.bloc;
  }

  static final ProviderMissingException =
      Exception("The Enterprise App couldn't be found");
}

class _EnterpriseAppState extends State<EnterpriseApp> {
  AppBloc bloc;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    bloc = AppBloc();
  }

  @override
  Widget build(BuildContext context) {
    return new _EnterpriseAppInherited(
      child: widget.child,
      bloc: bloc,
      config: widget.config,
    );
  }
}

class _EnterpriseAppInherited extends InheritedWidget {
  _EnterpriseAppInherited({
    Key key,
    @required Widget child,
    @required this.bloc,
    @required this.config,
  }) : super(key: key, child: child);

  final AppBloc bloc;
  final AppConfig config;

  @override
  bool updateShouldNotify(_EnterpriseAppInherited oldWidget) => false;
}

//Add further information here as needed.
class AppConfig {
  final Backend backend;

  final String host;
  final String container;
  final String processInstanceFragment;

  final EnterpriseTheme enterpriseTheme;

  AppConfig({
    @required this.backend,
    this.host = null,
    this.container = null,
    this.enterpriseTheme,
    this.processInstanceFragment,
  });
}

enum Backend {
  PAM,
  FIREBASE,
  SALESFORCE,
}
