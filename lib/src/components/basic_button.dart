import 'package:enterprise_core/src/components/enterprise_app.dart';
import 'package:flutter/material.dart';

bool isThin(BuildContext context) => MediaQuery.of(context).size.width <= 800;

class BasicButton extends FlatButton {
  final double width;
  @override
  final double height;
  BasicButton({
    @required VoidCallback onPressed,
    @required Widget child,
    @required this.width,
    this.height = 50,
  }) : super(
          onPressed: onPressed,
          child: child,
        );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      padding: const EdgeInsets.all(2.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(height / 2),
          shape: BoxShape.rectangle,
          color: EnterpriseApp.of(context).enterpriseTheme.primaryButtonColour),
      child: FlatButton(
        color: Colors.white,
        focusColor: Colors.white,
        splashColor: Colors.white,
        hoverColor: Colors.white,
        textColor:
            EnterpriseApp.of(context).enterpriseTheme.primaryButtonColour,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(height / 2)),
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
