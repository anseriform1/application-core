import 'dart:math';

import 'package:enterprise_core/src/bloc/process_bloc.dart';
import 'package:enterprise_core/src/components/enterprise_app.dart';
import 'package:enterprise_core/src/components/enterprise_task.dart';
import 'package:enterprise_core/src/models/user_processes.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:enterprise_core/src/repo/api.dart';
import 'package:flutter/material.dart';

//TODO: If we don't need to do anything on dispose of the widget, we can just make this an inherited widget.

class EnterpriseProcess extends StatefulWidget {
  EnterpriseProcess(
      {Key key,
      @required this.processId,
      @required this.child,
      @required this.taskList})
      : super(key: key);

  final String processId;
  final Widget child;
  final Map<String, TaskBuilder> taskList;

  @override
  _EnterpriseProcessState createState() => _EnterpriseProcessState();

  static ProcessBloc bloc(BuildContext context) {
    _EnterpriseProcessInherited provider = context
        .getElementForInheritedWidgetOfExactType<_EnterpriseProcessInherited>()
        ?.widget;

    if (provider == null) {
      throw ProviderMissingException;
    }

    return provider?.bloc;
  }

  static EnterpriseTask task(BuildContext context, PAMTask task,
      {PAMProcess process}) {
    _EnterpriseProcessInherited provider = context
        .getElementForInheritedWidgetOfExactType<_EnterpriseProcessInherited>()
        ?.widget;

    if (provider == null) {
      throw ProviderMissingException;
    }

    var taskName = task?.taskName ?? "New Task";

    if (!provider.taskList.containsKey(taskName)) {
      throw Exception("No Form Identified for Task Name $taskName");
    }

    return EnterpriseTask(
      key: new Key("${task?.taskId}"),
      task: task,
      taskId: task?.taskId,
      repo: provider.repo,
      child: provider.taskList[taskName](context, task, process: process),
    );
  }

  static final ProviderMissingException = Exception(
      '''Tasks can only be retrieved when the child of an Enterprise process.  
Please ensure that you have an EnterpriseProcess higher in your widget tree.

Generally wrapping the Route in a the EnterpriseTask is reccomended.
''');
}

class _EnterpriseProcessState extends State<EnterpriseProcess> {
  ProcessBloc bloc;
  ProcessRepo repo;

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new _EnterpriseProcessInherited(
      child: widget.child,
      bloc: bloc,
      repo: repo,
      taskList: widget.taskList,
    );
  }

  @override
  void initState() {
    super.initState();
    var config = EnterpriseApp.of(context);
    debugPrint("Building the widget now with host = ${config}");
    if (config == null) {
      throw Exception(
          '''Enterprise Process needs to be a child of an Enterprise App.  
Please ensure that you have an EnterpriseApp higher in your widget tree.

Generally wrapping the MaterialApp in a the Enterprise App is reccomended.
''');
    }
    var user = EnterpriseApp.bloc(context).username;

    switch (config.backend) {
      case Backend.PAM:
        repo = ProcessRepo(
          user: user,
          host: config.host,
          containerId: config.container,
          socketUrl:
              "ws://hasura-ingress-anseriform.4b63.pro-ap-southeast-2.openshiftapps.com/v1/graphql",
          instanceDetailsFragment: config.processInstanceFragment,
        );
        break;
      case Backend.FIREBASE:
        throw Exception("Firebase Repo not yet implemented");
        break;
      case Backend.SALESFORCE:
        throw Exception("Saleforce Repo not yet implemented");
        break;
    }

    bloc = ProcessBloc(repo: repo, processId: widget.processId);
  }
}

class _EnterpriseProcessInherited extends InheritedWidget {
  _EnterpriseProcessInherited({
    Key key,
    @required Widget child,
    @required this.bloc,
    @required this.taskList,
    @required this.repo,
  }) : super(key: key, child: child);

  final ProcessBloc bloc;
  final ProcessRepo repo;
  final Map<String, TaskBuilder> taskList;

  @override
  bool updateShouldNotify(_EnterpriseProcessInherited oldWidget) => false;
}

typedef TaskBuilder = Widget Function(BuildContext context, PAMTask task,
    {PAMProcess process});

class TabbedTasks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.04),
                blurRadius: 4.0,
                offset: Offset(2, 0))
          ]),
      width: getWidthOfTabBar(context),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          buildTaskListCard(context),
        ],
      ),
    );
  }

  double getWidthOfTabBar(BuildContext context) {
    final double preferredSize = MediaQuery.of(context).size.width / 5;
    debugPrint(preferredSize.toString());
    return max(300, preferredSize);
  }

  Widget buildTaskListCard(BuildContext context) {
    final bloc = EnterpriseProcess.bloc(context);
    final username = EnterpriseApp.bloc(context).username;
    final group = EnterpriseApp.bloc(context).company;
    final combinedStream = bloc.getProcessesByGroup(group);

    return StreamBuilder<List<PAMProcess>>(
      stream: combinedStream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Text("Error : ${snapshot.error}"),
            ),
          );
        }
        return Expanded(
          child: snapshot.hasData || snapshot.hasError
              ? buildTaskList(bloc, username, group, snapshot.data)
              : Center(child: CircularProgressIndicator()),
        );
      },
    );
  }

  Widget buildTaskList(
      ProcessBloc bloc, String user, String group, List<PAMProcess> tasks) {
    final userStream = bloc.getTasksByUser(user);
    final groupStream = bloc.getTasksForGroup(group);

    final userTaskCount = userStream.value?.length;
    final groupTaskCount = groupStream.value?.length;

    return StreamBuilder<PAMTask>(
        stream: bloc.currentTask,
        builder: (context, currTaskSnapshot) {
          return ListView.separated(
              separatorBuilder: (context, index) => Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 1.0,
                          color: Color(0xFFF0F0F0),
                        ),
                      ),
                      Container(width: 33.0)
                    ],
                  ),
              itemCount: tasks.length,
              itemBuilder: (context, index) {
                bool isSelected = false;
                final process = tasks[index];

                if (currTaskSnapshot.hasData) {
                  isSelected = (currTaskSnapshot.data?.taskId ==
                      process?.processTask?.taskId);
                }

                return Card(
                  margin: EdgeInsets.zero,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  color: isSelected ? Color(0xFFF6F6F6) : Colors.transparent,
                  child: (process?.processTask?.taskId == null)
                      ? buildListHeader(context, index,
                          index == 0 ? userTaskCount : groupTaskCount)
                      : ListTile(
                          onTap: () {
                            Navigator.popUntil(
                                context, ModalRoute.withName('/'));
                            debugPrint(
                                "Tapped ${process?.processTask?.taskId ?? "No ID"}");
                            bloc.selectTask(process.processTask);
                          },
                          title: Text(
                            "${process?.processTask?.taskName} : ${process?.processTask?.taskId}",
                            style: EnterpriseApp.of(context)
                                .enterpriseTheme
                                .titleTextStyle,
                          ),
                          subtitle: Text(
                              "${process?.processTask?.taskName ?? ""}  - ${process?.processTask?.taskStatus ?? ""}",
                              style: EnterpriseApp.of(context)
                                  .enterpriseTheme
                                  .subtitleTextStyle)),
                );
              });
        });
  }

  Widget buildListHeader(BuildContext context, int rowIndex, int count) {
    final isGroup = (rowIndex != 0);

    return Container(
        height: 70,
        color: Color(0xFFF1F1F1),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Image.asset(
                  (isGroup)
                      ? "assets/images/groupIcon.png"
                      : "assets/images/personalIcon.png",
                  package: "enterprise_core"),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(height: 6),
                  Text((isGroup) ? "Group Tasks" : "Personal Tasks",
                      style: EnterpriseApp.of(context)
                          .enterpriseTheme
                          .sidebarHeadingTitleStyle
                          .copyWith(color: Color(0xff606060))),
                  Text("There are $count Tasks",
                      style: EnterpriseApp.of(context)
                          .enterpriseTheme
                          .sidebarHeadingSubtitleStyle
                          .copyWith(color: Color(0xff606060))),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: InkWell(
                  onTap: () {
                    final processBloc = EnterpriseProcess.bloc(context);
                    final appBloc = EnterpriseApp.bloc(context);
                    (isGroup)
                        ? processBloc.createGroupProcessInstance(appBloc.group)
                        : processBloc.createUserProcessInstance(
                            appBloc.username, appBloc.group);
                  },
                  child: Image.asset(
                    "assets/images/addTaskButton.png",
                    package: "enterprise_core",
                  )),
            )
          ],
        ));
  }
}
