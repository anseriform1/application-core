import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/bloc/task_bloc.dart';
import 'package:enterprise_core/src/components/enterprise_app.dart';
import 'package:enterprise_core/src/models/user_tasks.dart';
import 'package:enterprise_core/src/repo/api.dart';
import 'package:flutter/material.dart';

//TODO: If we don't need to do anything on dispose of the widget, we can just make this an inherited widget.

class EnterpriseTask extends StatefulWidget {
  EnterpriseTask({
    Key key,
    @required this.child,
    @required this.taskId,
    @required this.task,
    @required this.repo,
  }) : super(key: key);

  final Widget child;
  final int taskId;
  final PAMTask task;
  final ProcessRepo repo;

  @override
  _EnterpriseTaskState createState() => _EnterpriseTaskState();

  static TaskBloc bloc(BuildContext context) {
    EnterpriseTaskInherited provider = context
        .getElementForInheritedWidgetOfExactType<EnterpriseTaskInherited>()
        ?.widget;
    return provider?.bloc;
  }
}

class _EnterpriseTaskState extends State<EnterpriseTask> {
  TaskBloc bloc;

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    var config = EnterpriseApp.of(context);

    bloc = TaskBloc(
      taskId: widget.task?.taskId,
      taskSummary: widget.task,
      autoUpdate: false,
      repo: widget.repo,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new EnterpriseTaskInherited(
      child: widget.child,
      bloc: bloc,
    );
  }
}

@visibleForTesting
class EnterpriseTaskInherited extends InheritedWidget {
  EnterpriseTaskInherited({
    Key key,
    @required Widget child,
    @required this.bloc,
  }) : super(key: key, child: child);

  final TaskBloc bloc;

  @override
  bool updateShouldNotify(EnterpriseTaskInherited oldWidget) => false;
}

class TaskFrame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = EnterpriseProcess.bloc(context);

    return StreamBuilder<PAMTask>(
      stream: bloc.currentTask,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return EnterpriseProcess.task(context, snapshot.data);
        } else {
          return EnterpriseProcess.task(context, snapshot.data);
        }
      },
    );
  }
}

class ProcessFrame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var bloc = EnterpriseProcess.bloc(context);

    return StreamBuilder<PAMProcess>(
      stream: bloc.currentProcess,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return EnterpriseProcess.task(context, snapshot.data.processTask,
              process: snapshot.data);
        } else {
          return EnterpriseProcess.task(context, null);
        }
      },
    );
  }
}
