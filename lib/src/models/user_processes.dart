// To parse this JSON data, do
//
//     final userTasks = userTasksFromJson(jsonString);

import 'dart:convert';

import 'user_tasks.dart';

UserProcesses userProcessesFromJson(String str) =>
    UserProcesses.fromJson(json.decode(str));

String userProcessesToJson(UserProcesses data) => json.encode(data.toJson());

class UserProcesses {
  List<PAMProcess> processes;

  UserProcesses({
    this.processes,
  });

  factory UserProcesses.fromJson(Map<String, dynamic> json) =>
      new UserProcesses(
        processes: new List<PAMProcess>.from(
            json["task-summary"].map((x) => PAMProcess.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "task-summary":
            new List<dynamic>.from(processes.map((x) => x.toJson())),
      };
}

class PAMProcess {
  String instanceId;
  String processId;
  int state;
  DateObject lastModificationDate;
  DateTime startDate;
  PAMTask processTask;
  Map<String, dynamic> data;
  List<Map<String, dynamic>> tasks;

  PAMProcess(
      {this.instanceId,
      this.processId,
      this.state,
      this.lastModificationDate,
      this.startDate,
      this.processTask,
      this.data});

  factory PAMProcess.fromGql(Map<String, dynamic> json) => new PAMProcess(
        instanceId: json.containsKey("processInstanceID")
            ? json["processInstanceID"].toString()
            : null,
        processId:
            json.containsKey("processId") ? json["processId"].toString() : null,
        state: json.containsKey("processInstanceState")
            ? json["processInstanceState"]
            : null,
        startDate: json.containsKey("startdate")
            ? DateTime.parse(json["startdate"])
            : null,
        lastModificationDate: json.containsKey("last-modification-date")
            ? DateObject.fromJson(json["last-modification-date"])
            : null,
        processTask: (json["activeUserTasks"] != null &&
                json["activeUserTasks"].length > 0)
            ? PAMTask.fromGql(json["activeUserTasks"][0])
            : null,
        data: json.containsKey('instanceDetails')
            ? json["instanceDetails"]
            : null,
      );

  factory PAMProcess.fromJson(Map<String, dynamic> json) => new PAMProcess(
      instanceId: json.containsKey("processInstanceID")
          ? json["processInstanceID"].toString()
          : null,
      processId:
          json.containsKey("processId") ? json["processId"].toString() : null,
      state: json.containsKey("processInstanceState")
          ? json["processInstanceState"]
          : null,
      startDate: json.containsKey("startdate")
          ? DateTime.parse(json["startdate"])
          : null,
      lastModificationDate: json.containsKey("last-modiffication-date")
          ? DateObject.fromJson(json["last-modiffication-date"])
          : null,
      processTask: json.containsKey('processTasks')
          ? PAMTask.fromJson(json["processTasks"][0])
          : null);

  Map<String, dynamic> toJson() => {
        "instanceid": instanceId,
        "processid": processId,
        "state": state,
        "startdate": startDate.toString(),
        "last-modification-date": lastModificationDate.toString(),
        "processTasks": processTask
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PAMProcess &&
          runtimeType == other.runtimeType &&
          instanceId == other.instanceId;

  @override
  int get hashCode => instanceId.hashCode;
}
