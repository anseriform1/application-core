// To parse this JSON data, do
//
//     final userTasks = userTasksFromJson(jsonString);

import 'dart:convert';

import 'package:intl/intl.dart';

UserTasks userTasksFromJson(String str) => UserTasks.fromJson(json.decode(str));

String userTasksToJson(UserTasks data) => json.encode(data.toJson());

class UserTasks {
  List<PAMTask> tasks;

  UserTasks({
    this.tasks,
  });

  factory UserTasks.fromJson(Map<String, dynamic> json) => new UserTasks(
        tasks: new List<PAMTask>.from(
            json["task-summary"].map((x) => PAMTask.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "task-summary": new List<dynamic>.from(tasks.map((x) => x.toJson())),
      };
}

class PAMTask {
  int taskId;
  String taskName;
  String taskSubject;
  String taskDescription;
  String taskStatus;
  int taskPriority;
  bool taskIsSkipable;
  String taskActualOwner;
  String taskCreatedBy;
  DateTime taskCreatedOn;
  DateTime taskActivationTime;
  dynamic taskExpirationTime;
  int taskProcInstId;
  String taskProcDefId;
  String taskContainerId;
  int taskParentId;
  Map<String, dynamic> data;
  List<String> taskPotOwners;

  PAMTask({
    this.taskId,
    this.taskName,
    this.taskSubject,
    this.taskDescription,
    this.taskStatus,
    this.taskPriority,
    this.taskIsSkipable,
    this.taskActualOwner,
    this.taskCreatedBy,
    this.taskCreatedOn,
    this.taskActivationTime,
    this.taskExpirationTime,
    this.taskProcInstId,
    this.taskProcDefId,
    this.taskContainerId,
    this.taskParentId,
    this.data,
    this.taskPotOwners,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PAMTask &&
          runtimeType == other.runtimeType &&
          taskId == other.taskId &&
          taskName == other.taskName &&
          taskSubject == other.taskSubject &&
          taskDescription == other.taskDescription &&
          taskStatus == other.taskStatus &&
          taskPriority == other.taskPriority &&
          taskIsSkipable == other.taskIsSkipable &&
          taskActualOwner == other.taskActualOwner &&
          taskCreatedBy == other.taskCreatedBy &&
          taskCreatedOn == other.taskCreatedOn &&
          taskActivationTime == other.taskActivationTime &&
          taskExpirationTime == other.taskExpirationTime &&
          taskProcInstId == other.taskProcInstId &&
          taskProcDefId == other.taskProcDefId &&
          taskContainerId == other.taskContainerId &&
          taskParentId == other.taskParentId &&
          data == other.data;

  @override
  int get hashCode =>
      taskId.hashCode ^
      taskName.hashCode ^
      taskSubject.hashCode ^
      taskDescription.hashCode ^
      taskStatus.hashCode ^
      taskPriority.hashCode ^
      taskIsSkipable.hashCode ^
      taskActualOwner.hashCode ^
      taskCreatedBy.hashCode ^
      taskCreatedOn.hashCode ^
      taskActivationTime.hashCode ^
      taskExpirationTime.hashCode ^
      taskProcInstId.hashCode ^
      taskProcDefId.hashCode ^
      taskContainerId.hashCode ^
      taskParentId.hashCode ^
      data.hashCode;

  factory PAMTask.fromGql(Map<String, dynamic> json) => new PAMTask(
        taskId: json.containsKey("taskId") ? json["taskId"] : null,
        taskName: json.containsKey("taskName") ? json["taskName"] : null,
        taskSubject:
            json.containsKey("taskSubject") ? json["taskSubject"] : null,
        taskDescription: json.containsKey("taskDescription")
            ? json["taskDescription"]
            : null,
        taskStatus: json.containsKey("taskStatus") ? json["taskStatus"] : null,
        taskPriority:
            json.containsKey("task-priority") ? json["task-priority"] : null,
        taskIsSkipable: json.containsKey("task-is-skipable")
            ? json["task-is-skipable"]
            : null,
        taskActualOwner: json.containsKey("taskActualOwner")
            ? json["taskActualOwner"]
            : null,
        taskCreatedBy:
            json.containsKey("taskCreatedBy") ? json["taskCreatedBy"] : null,
        taskCreatedOn: json.containsKey("taskCreatedOn")
            ? DateFormat("yyyy-MM-dd hh:mm:ss Z").parse(json["taskCreatedOn"])
            : null,
        taskActivationTime: json.containsKey("taskActivationTime")
            ? DateFormat("yyyy-MM-dd hh:mm:ss Z")
                .parse(json["taskActivationTime"])
            : null,
        taskExpirationTime: json.containsKey("taskExpirationTime")
            ? json["taskExpirationTime"]
            : null,
        taskProcInstId:
            json.containsKey("taskProcInstID") ? json["taskProcInstID"] : null,
        taskProcDefId:
            json.containsKey("taskProcDefID") ? json["taskProcDefID"] : null,
        taskContainerId: json.containsKey("taskContainerID")
            ? json["taskContainerID"]
            : null,
        taskParentId:
            json.containsKey("taskParentID") ? json["taskParentID"] : null,
        data: json.containsKey("taskInputData") ? json["taskInputData"] : null,
        taskPotOwners: json.containsKey("taskPotOwners")
            ? List<String>.from(json["taskPotOwners"]).map((e) => e).toList()
            : null,
      );

  factory PAMTask.fromJson(Map<String, dynamic> json) => new PAMTask(
        taskId: json.containsKey("taskID") ? json["taskID"] : null,
        taskName: json.containsKey("taskName") ? json["taskName"] : null,
        taskSubject:
            json.containsKey("task-subject") ? json["task-subject"] : null,
        taskDescription: json.containsKey("task-description")
            ? json["task-description"]
            : null,
        taskStatus: json.containsKey("taskStatus") ? json["taskStatus"] : null,
        taskPriority:
            json.containsKey("task-priority") ? json["task-priority"] : null,
        taskIsSkipable: json.containsKey("task-is-skipable")
            ? json["task-is-skipable"]
            : null,
        taskActualOwner:
            json.containsKey("actualowner_id") ? json["actualowner_id"] : null,
        taskCreatedBy: json.containsKey("task-created-by")
            ? json["task-created-by"]
            : null,
        taskCreatedOn: json.containsKey("startdate")
            ? DateTime.parse(json["startdate"])
            : null,
        taskActivationTime: json.containsKey("task-activation-time")
            ? DateTime.parse(json["task-activation-time"])
            : null,
        taskExpirationTime: json.containsKey("task-expiration-time")
            ? json["task-expiration-time"]
            : null,
        taskProcInstId: json.containsKey("task-proc-inst-id")
            ? json["task-proc-inst-id"]
            : null,
        taskProcDefId: json.containsKey("task-proc-def-id")
            ? json["task-proc-def-id"]
            : null,
        taskContainerId: json.containsKey("task-container-id")
            ? json["task-container-id"]
            : null,
        taskParentId:
            json.containsKey("task-parent-id") ? json["task-parent-id"] : null,
        taskPotOwners: json.containsKey("task-pot-owners")
            ? json["task-pot-owners"]
            : null,
      );

  Map<String, dynamic> toJson() => {
        "taskID": taskId,
        "taskName": taskName,
        "task-subject": taskSubject,
        "task-description": taskDescription,
        "taskStatus": taskStatus,
        "task-priority": taskPriority,
        "task-is-skipable": taskIsSkipable,
        "actualowner_id": taskActualOwner,
        "task-created-by": taskCreatedBy,
        "task-created-on": taskCreatedOn.toString(),
        "task-activation-time": taskActivationTime.toString(),
        "task-expiration-time": taskExpirationTime,
        "task-proc-inst-id": taskProcInstId,
        "task-proc-def-id": taskProcDefId,
        "task-container-id": taskContainerId,
        "task-parent-id": taskParentId,
        "task-pot-owners": taskParentId,
        "data": data,
      };
}

class DateObject {
  int javaUtilDate;

  DateObject({
    this.javaUtilDate,
  });

  factory DateObject.fromJson(Map<String, dynamic> json) => new DateObject(
        javaUtilDate: json["java.util.Date"],
      );

  Map<String, dynamic> toJson() => {
        "java.util.Date": javaUtilDate,
      };
}
