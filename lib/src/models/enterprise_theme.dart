import 'package:flutter/material.dart';

class EnterpriseTheme {
  Color primaryButtonColour = Color(0xFF545454);

  TextStyle sidebarHeadingTitleStyle;
  TextStyle sidebarHeadingSubtitleStyle;
  TextStyle titleTextStyle;
  TextStyle subtitleTextStyle;
  Color radioButtonColor;
  TextStyle inputTextLabelStyle;
  TextStyle inputTextStyle;
  TextStyle dropDownTextStyle;
  TextStyle dropDownLabelStyle;
  TextStyle buttonTextStyle;

  Color datePickerBkgdColour = Color(0xFF545454);
  Color appBarBkgdColour = Color(0xFF545454);

  EnterpriseTheme(
      {@required this.sidebarHeadingTitleStyle,
      @required this.sidebarHeadingSubtitleStyle,
      @required this.titleTextStyle,
      @required this.subtitleTextStyle,
      @required this.radioButtonColor,
      @required this.inputTextLabelStyle,
      @required this.inputTextStyle,
      @required this.dropDownTextStyle,
      @required this.dropDownLabelStyle,
      @required this.buttonTextStyle,
      this.datePickerBkgdColour,
      this.appBarBkgdColour});
}
