import 'package:enterprise_core/enterprise_core.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class MockRepo extends Mock implements TaskRepo {}

const TASK_ID = "TestTaskId";
const TEST_USER = "TestUser";
const TEST_GROUP = "TestGroup";
PAMTask TASK_SUMMARY = PAMTask(
  taskStatus: "NEW",
  taskName: "",
);
var TASK_DATA = <String, dynamic>{
  'customersummary': <String, dynamic>{
    'customername': 'customer1',
  },
//  'customersummary.customernumber': 789,
};

var TASK_DATA2 = <String, dynamic>{
  'customersummary': {'customername': 'customer2'},
};

void main() {
  group('Task bloc tests', () {
    test('Test setValue with new value', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(
        repo: taskRepo,
        autoUpdate: false,
      );
      bloc.setValue(TASK_DATA);

      expect(bloc.getCurrentOutputs(), {
        'customersummary': {'customername': 'customer1'}
      });
    });

    test('Test setValue with existing value', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(
        repo: taskRepo,
        autoUpdate: false,
      );
      bloc.register(property: 'customersummary.customername');
      debugPrint('properties : ' + bloc.properties.toString());
      bloc.setValue(TASK_DATA);
      debugPrint(bloc.getCurrentOutputs().toString());
    });

    test('Test setIndividualValue', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(
        repo: taskRepo,
        autoUpdate: false,
      );
      bloc.setIndividualValue('customersummary.customername', 'customer3');
      expect(bloc.getCurrentOutputs(),
          {'customersummary.customername': 'customer3'});
    });

    test('Test create task bloc with Task_Summary', () {
      //should call set value with task.data as task.data is not null
      MockRepo taskRepo = getTaskRepo(TASK_ID);
      TASK_SUMMARY.data = TASK_DATA;

      var bloc = TaskBloc(
          repo: taskRepo, autoUpdate: false, taskSummary: TASK_SUMMARY);

      expect(
          bloc.getCurrentOutputs(),
          {
            'customersummary': {'customername': 'customer1'}
          },
          reason:
              'task summary data is not null so it is pulled directly from the tasksummary.data');
    });

    test('Test create task bloc without Task_Summary', () {
      //should run repo.getTaskDetail and setvalue with the response as tasksummary.data is null but it has a task id
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false, taskId: TASK_ID);
      //TODO: is this a reasonable solution
      bloc.isLoadingStream.listen((value) {
        if (!value) {
          expect(
              bloc.getCurrentOutputs(),
              {
                'customersummary': {'customername': 'customer2'}
              },
              reason:
                  'task summary data is null so it pulls taskdetail from the repo which should reply with TASK_DATA2');
        }
      });
    });

    test('Test Bloc.register', () {
      //check that properties list gets updated
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false, taskId: TASK_ID);
      bloc.register(property: 'customersummary.customernumber');
      var prop = bloc.properties['customersummary.customernumber'];
      expect(bloc.properties, {'customersummary.customernumber': prop});
    });

    test('Test setNestedValueIntoMap', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false);

      expect(
          bloc.setNestedValueIntoMap(
              [
                'customersummary',
                'customername',
              ],
              'customer1',
              {}),
          {
            'customersummary': {'customername': 'customer1'}
          });
    });

    test('Test mapFromValue', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false);
      bloc.register<num>(property: 'customersummary.customernumber');
      bloc.register(property: 'customersummary.customercompany');
      bloc.properties['customersummary.customernumber'].valueSink.add(321);
      bloc.properties['customersummary.customercompany'].valueSink
          .add('grinders');
      //TODO: works with string not num, even with <num>
      debugPrint(bloc.mapFromValue(TASK_DATA).toString());
    });

    test('Test output Stream', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false);
      bloc.setValue(TASK_DATA);
      bloc.outputStream.listen((event) {
        expect(event, TASK_DATA);
      });
    });

    test('Test get current outputs', () {});

    test('Test get collapse', () {
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var datamap = {
        'customersummary': {'customername': 'ben', 'customernumber': '123'}
      };

      var bloc = TaskBloc(repo: taskRepo, autoUpdate: false);
      bloc.setValue(datamap);
    });

    //test all task Actions (complete, claim, release, start)
    //this includes testing the repo actions and the associated handle method which can be done by checking task summary is updated

    test('Test startTask', () async {
      TASK_SUMMARY.taskStatus = 'NEW';
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(
          repo: taskRepo, autoUpdate: false, taskSummary: TASK_SUMMARY);
      expect(bloc.taskSummary.taskStatus, 'NEW');
      await bloc.startTask(TEST_USER).first;
      expect(bloc.taskSummary.taskStatus, 'InProgress');
    });

    test('Test claimTask', () async {
      TASK_SUMMARY.taskStatus = 'NEW';
      MockRepo taskRepo = getTaskRepo(TASK_ID);
      var bloc = TaskBloc(
          repo: taskRepo, autoUpdate: false, taskSummary: TASK_SUMMARY);
      expect(bloc.taskSummary.taskStatus, 'NEW');
      await bloc.claimTask(TEST_USER).first;
      expect(bloc.taskSummary.taskStatus, 'Reserved');
    });

    test('Test createTask', () async {
      TASK_SUMMARY.taskStatus = 'NEW';
      MockRepo taskRepo = getTaskRepo(TASK_ID);

      var bloc = TaskBloc(
          repo: taskRepo, autoUpdate: false, taskSummary: TASK_SUMMARY);
      expect(bloc.taskSummary.taskStatus, 'NEW');
      await bloc.releaseTask(TEST_USER).first;
      expect(bloc.taskSummary.taskStatus, 'Ready');
    });
  });
}

MockRepo getTaskRepo(String processId) {
  var taskRepo = MockRepo();

  when(taskRepo.getTaskInput())
      .thenAnswer((_) => Stream.fromIterable([TASK_DATA2]));

  when(taskRepo.startTask(any)).thenAnswer((_) => Stream.value(true));
  when(taskRepo.claimTask(any)).thenAnswer((_) => Stream.value(true));
  when(taskRepo.releaseTask(any)).thenAnswer((_) => Stream.value(true));

  return taskRepo;
}
