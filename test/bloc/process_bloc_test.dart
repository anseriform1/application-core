import 'package:enterprise_core/enterprise_core.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

class MockRepo extends Mock implements ProcessRepo {}

const PROCESS_ID = "TestProcessId";
const TEST_USER = "TestUser";
const TEST_GROUP = "TestGroup";
const TEST_DATA = <String, dynamic>{
  'taskID': 7,
  'taskName': 'Manager Approves or Rejects',
  'actualowner_id': 'ben.plunkett@seventhbeam.com'
};

var emptyTaskList = [PAMTask()];
var taskList = List.generate(4, (i) => PAMTask(taskId: "TASK$i"));

void main() {
  group('Process bloc tests', () {
    test('Calling get tasks for User, gets taks for user from repo', () async {
      MockRepo processRepo = getProcessRepo(TEST_USER);

      var bloc = ProcessBloc(
        processId: PROCESS_ID,
        repo: processRepo,
      );

      var stream = bloc.getTasksByUser(TEST_USER);
      verify(processRepo.getTasksByUser(TEST_USER));

      expect(stream, emitsInOrder([emptyTaskList, taskList]),
          reason:
              "Initial Task List should be empty, followed by a list of tasks");
    });

    test('Calling get tasks for Group, gets taks for group from repo', () {
      MockRepo processRepo = getProcessRepo(TEST_USER);

      var bloc = ProcessBloc(
        processId: PROCESS_ID,
        repo: processRepo,
      );

      var stream = bloc.getTasksForGroup(TEST_GROUP);

      verify(processRepo.getTasksForGroup(TEST_GROUP));
      expect(stream, emitsInOrder([taskList, emptyTaskList]));
    });

    test('Calling select task with the first task in taskList', () {
      MockRepo processRepo = getProcessRepo(TEST_USER);

      var bloc = ProcessBloc(
        processId: PROCESS_ID,
        repo: processRepo,
      );

      expect(bloc.currentTask.value, null);
      bloc.selectTask(taskList[0]);
      expect(bloc.currentTask.value, taskList[0]);
    });

    test('Mark task completed', () {
      MockRepo processRepo = getProcessRepo(TEST_USER);
      var bloc = ProcessBloc(
        processId: PROCESS_ID,
        repo: processRepo,
      );

      expect(bloc.currentTask.value, null,
          reason:
              'Group tasks should be empty so current task will be set to null');
      bloc.getTasksForGroup(TEST_GROUP);
      bloc.markTaskCompleted(TEST_USER);
      expect(bloc.currentTask.value, bloc.getTasksForGroup(TEST_GROUP).value[0],
          reason: 'current tasks should be set to first task in groupTasks');
    });

    test('create Group Process Instance', () {
      MockRepo processRepo = getProcessRepo(PROCESS_ID);
      var bloc = ProcessBloc(repo: processRepo, processId: PROCESS_ID);
      bloc.createGroupProcessInstance(TEST_GROUP);
      verify(processRepo.createGroupProcessInstance(PROCESS_ID, TEST_GROUP));
    });

    test('create User Process Instance', () {
      MockRepo processRepo = getProcessRepo(PROCESS_ID);
      var bloc = ProcessBloc(repo: processRepo, processId: PROCESS_ID);

      expect(
          bloc.createUserProcessInstance(PROCESS_ID, TEST_GROUP,
              data: TEST_DATA),
          emitsInOrder(['1']));
    });
  });
}

MockRepo getProcessRepo(String processId) {
  var processRepo = MockRepo();

  when(processRepo.getTasksByUser(any)).thenAnswer(
    (_) => Stream.fromIterable([emptyTaskList, taskList]),
  );

  when(processRepo.getTasksForGroup(TEST_GROUP)).thenAnswer(
    (_) => Stream.fromIterable([taskList, emptyTaskList]),
  );

  when(processRepo.createGroupProcessInstance(processId, TEST_GROUP))
      .thenAnswer((_) => Stream.fromIterable(['1']));

  when(processRepo.createUserProcessInstance(processId, any, input: TEST_DATA))
      .thenAnswer((_) => Stream.fromIterable(['1']));

  return processRepo;
}
