import 'package:enterprise_core/enterprise_core.dart';
import 'package:test/test.dart';

void main() {
  const USERNAME = 'testuser1';
  const GROUP = 'testgroup1';
  const COMPANY = 'testcompany1';

  group('app bloc tests', () {
    test('set username', () {
      var bloc = AppBloc();
      expect(bloc.username, null);
      bloc.setUsername(USERNAME);
      expect(bloc.username, USERNAME,
          reason: 'bloc.username should equal USERNAME');
    });

    test('set group', () {
      var bloc = AppBloc();
      expect(bloc.group, null);
      bloc.setGroup(GROUP);
      expect(bloc.group, GROUP, reason: 'bloc.group should equal GROUP');
    });

    test('set data', () {
      var bloc = AppBloc();
      expect(bloc.username, null);
      expect(bloc.group, null);
      expect(bloc.company, null);
      expect(bloc.isLoggedInStream, emits(false));
      bloc.setData(USERNAME, GROUP, COMPANY);
      expect(bloc.username, USERNAME);
      expect(bloc.group, GROUP);
      expect(bloc.company, COMPANY);
      expect(bloc.isLoggedInStream, emits(true));
    });

    test('logout', () {
      var bloc = AppBloc();
      bloc.setData(USERNAME, GROUP, COMPANY);
      expect(bloc.username, USERNAME);
      expect(bloc.group, GROUP);
      expect(bloc.company, COMPANY);
      expect(bloc.isLoggedInStream, emits(true));
      bloc.logout();
      expect(bloc.username, null);
      expect(bloc.group, null);
      expect(bloc.company, null);
      expect(bloc.isLoggedInStream, emits(false));
    });
  });
}
