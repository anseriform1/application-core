import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/bloc/task_bloc.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';

class MockBloc extends Mock implements TaskBloc {}

class TestTask extends StatefulWidget {
  final Widget child;
  final TaskBloc bloc;
  final PAMTask task;

  TestTask({Key key, this.child, this.bloc, this.task}) : super(key: key);

  @override
  State<TestTask> createState() => _TestTaskState();
}

class _TestTaskState extends State<TestTask> {
  @override
  Widget build(BuildContext context) {
    return new EnterpriseTaskInherited(
      child: widget.child,
      bloc: widget.bloc,
    );
  }
}

class TestHarness extends StatelessWidget {
  final Widget child;
  final TaskBloc bloc;
//  final String taskStatus;

  const TestHarness({Key key, this.child, this.bloc}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return EnterpriseApp(
      config: AppConfig(
        backend: Backend.PAM,
        enterpriseTheme: EnterpriseTheme(
          subtitleTextStyle: TextStyle(fontSize: 10),
          titleTextStyle: TextStyle(fontSize: 10),
        ),
      ),
      child: MaterialApp(
        home: Scaffold(
          body: EnterpriseProcess(
            processId: "TESTPROCESS",
            taskList: {},
            child: TestTask(
              bloc: bloc,
              task: PAMTask(
                taskId: "TEST_TASK",
                taskStatus: "wew",
              ),
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
