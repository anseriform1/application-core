// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:enterprise_core/enterprise_core.dart';
import 'package:enterprise_core/src/components/basic_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:rxdart/rxdart.dart';

import '../TestTools.dart';

void main() {
  PAMTask TEST_TASK = PAMTask(taskStatus: "New", taskId: '123');
  BehaviorSubject<PAMTask> currentTask = BehaviorSubject<PAMTask>();

  var testBloc = MockBloc();

  testWidgets('Form Card displays task ID', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          child: Center(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text(' - Task # 123'), findsOneWidget);
  });

  testWidgets('Form Card displays correct title', (WidgetTester tester) async {
    //TODO: Implement a better test? or fix FormCard?
    tester.binding.window.physicalSizeTestValue = Size(900, 500);
    tester.binding.window.devicePixelRatioTestValue = 1.0;
    TEST_TASK.taskName = "Manager Approves or Rejects?";
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          child: Column(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text("Manager Approves or Rejects?"), findsOneWidget);
  });

  testWidgets('Action buttons enabled', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          showActionButtons: true,
          child: Container(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text('Create', skipOffstage: false), findsOneWidget);
    expect(find.widgetWithText(BasicButton, 'Create', skipOffstage: false),
        findsOneWidget);
  });

  testWidgets('Action buttons disbaled', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          showActionButtons: false,
          child: Container(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text('Create', skipOffstage: false), findsNothing);
    expect(find.widgetWithText(BasicButton, 'Create'), findsNothing);
  });

  testWidgets('Null task draws cancel button', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(null);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          showActionButtons: false,
          child: Container(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text('Cancel', skipOffstage: false), findsOneWidget);
    expect(find.byIcon(Icons.close, skipOffstage: false), findsOneWidget);
  });

  testWidgets('Not null task draws create new button',
      (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          showActionButtons: false,
          child: Container(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    expect(find.text('Create New', skipOffstage: false), findsOneWidget);
    expect(find.byIcon(Icons.add, skipOffstage: false), findsOneWidget);
  });

  testWidgets('Test Admin Group', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(TEST_TASK);
    when(testBloc.currentTaskSummaryStream).thenAnswer((_) => currentTask);
    currentTask.add(TEST_TASK);

    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: FormCard(
          showActionButtons: false,
          child: Container(),
        ),
      ),
    );

    await tester.pump(Duration.zero);

    //TODO: Figure out how to access AppBloc and set group in test
    // expect(find.text('Create New', skipOffstage: false), findsNothing);
    // expect(find.byIcon(Icons.add, skipOffstage: false), findsNothing);
  });
}
