// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:enterprise_core/enterprise_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../TestTools.dart';

void main() {
  var testBloc = MockBloc();

  testWidgets('New Task has Create Buttons', (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(PAMTask(taskStatus: "New"));
    // Build our app and trigger a frame.
    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: ActionButtons(),
      ),
    );

    // Verify that our counter starts at 0.
    expect(find.text('Create'), findsOneWidget);
  });

  testWidgets('Reserve Task has Start, Release Buttons',
      (WidgetTester tester) async {
    when(testBloc.taskSummary).thenReturn(PAMTask(taskStatus: "Reserved"));
    // Build our app and trigger a frame.
    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: ActionButtons(),
      ),
    );

    // Verify that our counter starts at 0.
    expect(find.text('Start'), findsOneWidget);
    expect(find.text('Release'), findsOneWidget);
  });

  testWidgets('InProgress Task has Create Buttons',
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    when(testBloc.taskSummary).thenReturn(PAMTask(taskStatus: "InProgress"));
    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: ActionButtons(),
      ),
    );

    // Verify that our counter starts at 0.
    expect(find.text('Release'), findsOneWidget);
    expect(find.text('Complete'), findsOneWidget);
  });

  testWidgets('Ready Task has Create Buttons', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    when(testBloc.taskSummary).thenReturn(PAMTask(taskStatus: "Ready"));
    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: ActionButtons(),
      ),
    );

    // Verify that our counter starts at 0.
    expect(find.text('Claim'), findsOneWidget);
  });
}
