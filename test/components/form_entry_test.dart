// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:enterprise_core/src/components/basic_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../TestTools.dart';

void main() {
  var testBloc = MockBloc();

  //TODO: test all form entries

  testWidgets('Test form entries', (WidgetTester tester) async {
    await tester.pumpWidget(
      TestHarness(
        bloc: testBloc,
        child: BasicButton(
          child: Text('test button'),
          width: 100,
        ),
      ),
    );

    expect(find.widgetWithText(FlatButton, 'test button'), findsOneWidget);
  });
}
